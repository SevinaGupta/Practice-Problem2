const items = [{
    name: 'Orange',
    available: true,
    contains: "Vitamin C"
}, {
    name: 'Mango',
    available: true,
    contains: "Vitamin K, Vitamin C"
}, {
    name: 'Pineapple',
    available: true,
    contains: "Vitamin A",
}, {
    name: 'Raspberry',
    available: false,
    contains: "Vitamin B, Vitamin A"

}, {
    name: 'Grapes',
    contains: "Vitamin D",
    available: false
}]



const test = require("./problem.js")

test.availableItemsByReduce(items)
test.availableItemsByMap(items)
test.containVitaminC(items)
test.containVitaminA(items)
test.groupedByContains(items)
test.sortDataBasedOnVitamins(items)