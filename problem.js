/*



    Q. Get all items that are available (using both Map and Reduce)
    Q. Get all items containing only Vitamin C.
    Q. Get all items containing Vitamin A.
    Q. Group Items based on Vitamins that they contain.
        If an item contains more than 1 vitamin ..
        Put the copy of the item in each of them.
    Q. Sort items based on number of vitamins they contain.

*/
function availableItemsByReduce(items){
	const availableItems = items.reduce((curr,pre)=>{
		const{available} = pre
		if(available == true){
			console.log(pre)
		}
 },{})
 return availableItems
}


function availableItemsByMap(items){
	const availableItems = items.map((element)=>{
		const{available}= element
		if(available==true){
			console.log(element)
		}
	})
}

function containVitaminC(items){
	const vitaminC = items.forEach(element => {
		const {contains} = element
		if(contains == "Vitamin C"){
			console.log(element)
		}
	});
}


function containVitaminA(items){
	const vitaminC = items.forEach(element => {
		const {contains} = element
		if(contains.includes("Vitamin A")){
			console.log(element)
		}
	});
}


function groupedByContains(items){

	const groupedData = items.reduce((curr, pre)=>{
		 let key = pre['contains']
          let splitKey = key.split(",")
		  for(let value of splitKey){
			if(curr[value]){		
				curr[value].push(pre)	 
			 }
			 else{
				curr[value]=[]
				curr[value].push(pre)
	
			}
		  }
		
		return curr
	},{})
	 console.log(groupedData)
	return groupedData
}


function sortDataBasedOnVitamins(items){
	let arr = []
	items.forEach((element)=>{
		arr.push(element)
	})
		arr.sort((a,b)=>{
			if(a.contains.length < b.contains.length){
				return 1
			}
			if(a.contains.length>b.contains.length){
				return -1
			}
			else{
				return 0
			}
		})
		console.log(arr)

	
}

module.exports = {availableItemsByReduce,availableItemsByMap,containVitaminC,containVitaminA,groupedByContains,sortDataBasedOnVitamins}



